<?
//please enter your mysql server(usually localhost), username and password below 
$server="localhost";
$username="user_user";
$password="pass";

//database name:
$db_name="user_database";

//table name:
$tbl="feedback";

//field names:
$note="note";
$date="ndate";
$id="id";

//Please enter the message you would like to appear on the popup below:
//this can only be plain text and has to be all in one line
//to use newline(enter), just put \\n for every new line (equivalent to <br>)
$popup="****************************************************\\n\\nWhy are you leaving without giving us a try?\\nPlease let us know so we can improve our service.\\n\\n*****************************************************";

//And here please enter the thankyou message/html code (after user submits feedback)
$thanks='<center><BR><b><font color="#CC0000" face="Tahoma" size="4">Thank you for submitting your feedback!</font><p><font face="Verdana" size="2">For More Great Offers You Can Visit My Special Page Below.<br></font><font face="Verdana"><a href="http://www.yourwebsite.com/?">Click Here!</a></font></p></center>';

//switch for the popup - if you want to disable it, simply uncomment the following line
//$z=0;

//admin page username and password:
$valid_user = "admin";
$valid_pass = "admin";
?>